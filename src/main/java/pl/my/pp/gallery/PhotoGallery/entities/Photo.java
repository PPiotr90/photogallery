package pl.my.pp.gallery.PhotoGallery.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "photos")
public class Photo {
    @ManyToOne
    @JoinColumn(name = "gallery_id")
    Gallery gallery;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int id;
    @Column
    private String value;

    @Override
    public String toString() {
        return "Photo{" +
                "gallery=" + gallery +
                ", id=" + id +
                ", value='" + value + '\'' +
                '}';
    }
}
