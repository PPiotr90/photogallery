package pl.my.pp.gallery.PhotoGallery.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DtoPhoto {
    private String value;
    private String galleryId;

}
