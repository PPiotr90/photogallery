package pl.my.pp.gallery.PhotoGallery.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.my.pp.gallery.PhotoGallery.entities.Role;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
