package pl.my.pp.gallery.PhotoGallery.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.my.pp.gallery.PhotoGallery.entities.Gallery;
import pl.my.pp.gallery.PhotoGallery.entities.Photo;
import pl.my.pp.gallery.PhotoGallery.entities.dao.GalleryRepository;
import pl.my.pp.gallery.PhotoGallery.entities.dao.PhotoRepository;
import pl.my.pp.gallery.PhotoGallery.entities.dao.RoleRepository;
import pl.my.pp.gallery.PhotoGallery.entities.dao.UserRepository;

import java.util.List;

@Controller
@RequestMapping("/photos")
public class PhotoController {

    private PhotoRepository photoRepository;
    private UserRepository userRepository;
    private GalleryRepository galleryRepository;
    private PasswordEncoder passwordEncoder;
    private RoleRepository roleRepository;
    private boolean afterSuccessAdd = false;

    @Autowired
    public PhotoController(PhotoRepository photoRepository, UserRepository userRepository, GalleryRepository galleryRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.photoRepository = photoRepository;
        this.userRepository = userRepository;
        this.galleryRepository = galleryRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;

    }

    @GetMapping("/list")
    public String getPhotos(Model theModel, Authentication authentication) {
        String userName = authentication.getName();
        final List<Photo> listOfPhotos = photoRepository.findPhotosByUsername(userName);
        final Gallery galleryByUsername = galleryRepository.findGalleryByUsername(userName);
        theModel.addAttribute("gallery", galleryByUsername);
        theModel.addAttribute("photos", listOfPhotos);
        return "list-photos";

    }


}

