package pl.my.pp.gallery.PhotoGallery.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DtoObject {

    private String userName;
    private String password;
    private String galleryName;
    private String photoValues;

    public DtoObject(String userName, String password, String galleryName, String photoValues) {
        this.userName = userName;
        this.password = password;
        this.galleryName = galleryName;
        this.photoValues = photoValues;
    }
}
