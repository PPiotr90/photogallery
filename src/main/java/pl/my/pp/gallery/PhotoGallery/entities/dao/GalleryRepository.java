package pl.my.pp.gallery.PhotoGallery.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.my.pp.gallery.PhotoGallery.entities.Gallery;

@Repository
@Transactional
public interface GalleryRepository extends JpaRepository<Gallery, Integer> {

    @Query("select g from Gallery g where  g.username = :username")
    Gallery findGalleryByUsername(@Param(("username")) String username);
}
