package pl.my.pp.gallery.PhotoGallery.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.my.pp.gallery.PhotoGallery.entities.Photo;

import java.util.List;

@Repository
@Transactional
public interface PhotoRepository extends JpaRepository<Photo, Integer> {

    @Query("select p from Photo p join p.gallery g where g.username = :username")
    List<Photo> findPhotosByUsername(@Param("username") String username);
}
