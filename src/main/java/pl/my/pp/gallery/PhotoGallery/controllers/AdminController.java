package pl.my.pp.gallery.PhotoGallery.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.my.pp.gallery.PhotoGallery.dto.DtoObject;
import pl.my.pp.gallery.PhotoGallery.dto.DtoPhoto;
import pl.my.pp.gallery.PhotoGallery.entities.Gallery;
import pl.my.pp.gallery.PhotoGallery.entities.Photo;
import pl.my.pp.gallery.PhotoGallery.entities.Role;
import pl.my.pp.gallery.PhotoGallery.entities.User;
import pl.my.pp.gallery.PhotoGallery.entities.dao.GalleryRepository;
import pl.my.pp.gallery.PhotoGallery.entities.dao.PhotoRepository;
import pl.my.pp.gallery.PhotoGallery.entities.dao.RoleRepository;
import pl.my.pp.gallery.PhotoGallery.entities.dao.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    UserRepository userRepository;
    GalleryRepository galleryRepository;
    PhotoRepository photoRepository;
    RoleRepository roleRepository;
    boolean afterSuccessAdd = false;

    @Autowired
    public AdminController(UserRepository userRepository, GalleryRepository galleryRepository,
                           PhotoRepository photoRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.galleryRepository = galleryRepository;
        this.photoRepository = photoRepository;
        this.roleRepository = roleRepository;
    }


    @GetMapping("/adm")
    public String getHome(Model theModel) {


        final List<Gallery> listOfGaleries = galleryRepository.findAll();
        theModel.addAttribute("galleries", listOfGaleries);
        theModel.addAttribute("newuser", new DtoObject());
        theModel.addAttribute("newPhoto", new DtoPhoto());
        checkToAddSuccessAfterAdd(theModel);
        return "/administrator-panel";

    }


    @PostMapping("/newuser")
    public String addUser(@ModelAttribute("newuser") DtoObject dtoObject) {
        User newUser = new User();
        newUser.setUserName(dtoObject.getUserName());
        newUser.setPassword(dtoObject.getPassword());
        newUser.setEnabled(1);
        Gallery newGallery = new Gallery();
        newGallery.setName(dtoObject.getGalleryName());
        newGallery.setUsername(newUser.getUserName());
        Role newRole = new Role();
        newRole.setUsername(dtoObject.getUserName());
        newRole.setAuthority("ROLE_USER");
        roleRepository.save(newRole);
        final String[] photos = dtoObject.getPhotoValues().split(",");
        galleryRepository.save(newGallery);
        List<Photo> newPhotos = new ArrayList<>();
        for (String s : photos) {
            Photo photo = new Photo();
            photo.setValue(s);
            photoRepository.save(photo);

            newGallery.getPhotos().add(photo);
            photo.setGallery(newGallery);
            newPhotos.add(photo);

        }
        photoRepository.saveAll(newPhotos);
        galleryRepository.save(newGallery);
        userRepository.save(newUser);
        afterSuccessAdd = true;
        afterSuccessAdd = true;

        return "redirect:/admin/adm";
    }

    @PostMapping("/addPhoto")
    public String addPhoto(@ModelAttribute("newPhoto") DtoPhoto newPhoto) {
        final Gallery galleryFromBase = galleryRepository.findById(Integer.parseInt(newPhoto.getGalleryId()))
                .orElse(null);
        if (galleryFromBase != null) {
            Photo phtotoSave = new Photo();
            phtotoSave.setValue(newPhoto.getValue());
            phtotoSave.setGallery(galleryFromBase);
            galleryFromBase.getPhotos().add(phtotoSave);
            photoRepository.save(phtotoSave);
            galleryRepository.save(galleryFromBase);
        }


        return "redirect:/admin/adm";


    }


    private void checkToAddSuccessAfterAdd(Model theModel) {
        if (afterSuccessAdd) {
            afterSuccessAdd = false;
            theModel.addAttribute("SuccessMessage", "Succeddfully create user!");
        }
    }
}
